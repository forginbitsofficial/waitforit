using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InterSceneController : MonoBehaviour
{
    private void Awake()
    {
        GameData.CurrentLevel++;

    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WaitAndGoToNextMinigame());


    }

    IEnumerator WaitAndGoToNextMinigame()
    {
        //yield return new WaitForSeconds((3f / (1 + (GameData.CurrentLevel + 1) / 20f)));
        yield return new WaitForSeconds(3f);

        if (GameData.LivesRemaining <= 0)
        {
            if (GameObject.FindGameObjectWithTag("LevelTransitioner") != null)
            {
                GameObject.FindGameObjectWithTag("LevelTransitioner").GetComponent<LevelTransitioner>().FadeIn("GameOverScene");
            }
            else
            {
                SceneManager.LoadScene("GameOverScene");
            }

        }
        else
        {

            int randomMinigame;

            //do { randomMinigame = UnityEngine.Random.Range(1, 11); }
            //while (randomMinigame == GameData.PreviousMinigameIndex);

            do { randomMinigame = UnityEngine.Random.Range(1, 11); }
            while (GameData.PreviousMinigames.Contains(randomMinigame));

            //GameData.PreviousMinigameIndex = randomMinigame;

            GameData.PreviousMinigames.Add(randomMinigame);

            if (GameData.PreviousMinigames.Count > 9)
            {
                for (int i = 0; i < 8; i++)
                {
                    GameData.PreviousMinigames.RemoveAt(0);
                }
            }

            if (GameObject.FindGameObjectWithTag("LevelTransitioner") != null)
            {
                GameObject.FindGameObjectWithTag("LevelTransitioner").GetComponent<LevelTransitioner>().FadeIn("MiniGame" + randomMinigame.ToString());
            }
            else
            {
                SceneManager.LoadScene("MiniGame" + randomMinigame.ToString());
            }

        }

    }
}
