using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceSound : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Cake"))
        {
            if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
            {
                Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
                sound.GetComponent<AudioSource>().PlayOneShot(
                    sound.GetComponent<SoundSource>().Sounds[1]
                    );
            }
        }
     
    }
}
