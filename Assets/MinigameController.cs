using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MinigameController : MonoBehaviour
{
    public bool HasFailed = false;

    public Slider timeSlider;

    public float InitialSeconds;
    public float CurrentSeconds;

    public bool isFinished = false;

    // Start is called before the first frame update
    void Start()
    {
        AdjustDifficultySettings(GameData.CurrentLevel);

        Time.timeScale = (1 + (GameData.CurrentLevel + 1) / 40f);
        Debug.Log("timescale : " + Time.timeScale);

        //InitialSeconds = (10f / (1 + (GameData.CurrentLevel + 1) / 5f));
        InitialSeconds = 10f;
        CurrentSeconds = InitialSeconds;
        //StartCoroutine(WaitAndGoToInterScene());

        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
            sound.GetComponent<AudioSource>().PlayOneShot(
                sound.GetComponent<SoundSource>().Sounds[3]
                );
        }
    }

    private void Update()
    {
        if (CurrentSeconds - Time.deltaTime <= 0)
        {
            CurrentSeconds = 0;

            FinishMinigame();
        }
        else
        {
            CurrentSeconds -= Time.deltaTime;
        }

        timeSlider.value = CurrentSeconds / InitialSeconds;
    }

    private void FinishMinigame()
    {
        if (!isFinished)
        {
            isFinished = true;

            if (HasFailed)
            {
                GameData.LivesRemaining--;
            }

            if (GameObject.FindGameObjectWithTag("LevelTransitioner") != null)
            {
                GameObject.FindGameObjectWithTag("LevelTransitioner").GetComponent<LevelTransitioner>().FadeIn("InterScene");
            }
            else
            {
                SceneManager.LoadScene("InterScene");
            }
        }

    }

    private void AdjustDifficultySettings(int currentLevel)
    {

    }

    //IEnumerator WaitAndGoToInterScene()
    //{

    //    Debug.Log(5f / (GameData.CurrentLevel + 1));
    //    //yield return new WaitForSeconds();


    //}
}
