using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottleSpawner : MonoBehaviour
{
    public MinigameController mgcontroller;

    public GameObject bottlePrefab;

    public Vector3 force;
    public float torque;

    public float waitTime;

    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(ThrowBottle());
    }

    //IEnumerator ThrowBottle()
    //{

    //    GameObject bottle = Instantiate(bottlePrefab, transform.position, Quaternion.identity);

    //    bottle.GetComponent<Rigidbody2D>().AddForce(force, ForceMode2D.Impulse);
    //    bottle.GetComponent<Rigidbody2D>().AddTorque(torque, ForceMode2D.Impulse);
    //    bottle.GetComponent<DeathCollider>().controller = mgcontroller;

    //    yield return new WaitForSeconds(waitTime);

    //    StartCoroutine(ThrowBottle());

    //}

    internal void Shoot()
    {
        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
            sound.GetComponent<AudioSource>().PlayOneShot(
                sound.GetComponent<SoundSource>().Sounds[12]
                );
        }

        GameObject bottle = Instantiate(bottlePrefab, transform.position, Quaternion.identity);

        bottle.GetComponent<Rigidbody2D>().AddForce(force, ForceMode2D.Impulse);
        bottle.GetComponent<Rigidbody2D>().AddTorque(torque, ForceMode2D.Impulse);
        bottle.GetComponent<DeathCollider>().controller = mgcontroller;
    }
}
