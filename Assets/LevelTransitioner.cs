using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelTransitioner : MonoBehaviour
{
    string sceneToLoad;

    Image img;

    public float timeToFade;

    internal void FadeIn(string linkToScene)
    {
      
        Debug.Log("fadein");

        sceneToLoad = linkToScene;

        // START IN

        img = transform.Find("Canvas").Find("Image").GetComponent<Image>();

        LeanTween.value(gameObject, 0f, 1f, timeToFade).setOnUpdate(
            (float val) => { img.color = new Color(0f, 0f, 0f, val); }).setOnComplete(LoadNextScene);

        // LOAD SCENE


        // FADE OUT
    }

    private void LoadNextScene()
    {
        // Use a coroutine to load the Scene in the background
        StartCoroutine(LoadYourAsyncScene());

    }

    IEnumerator LoadYourAsyncScene()
    {
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneToLoad);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        FadeOut();
    }

    private void FadeOut()
    {

        LeanTween.value(gameObject, 1f, 0f, timeToFade).setOnUpdate(
                   (float val) => { img.color = new Color(0f, 0f, 0f, val); });

      
    }

   
}
