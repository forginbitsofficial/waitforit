using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateRaise : MonoBehaviour
{

    //public float tiltIncrement = 1f;

    //public float tiltForce = 1f;

    //public Rigidbody2D plateRb;

    public int nbSlaps = 0;

    public MinigameController controller;

    public List<GameObject> bananas;

    public GameObject cakeShine;

    private void Start()
    {
        controller.HasFailed = true;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            nbSlaps++;

            if (bananas.Count > 0)
            {
                bananas[bananas.Count - 1].GetComponent<Banana>().Drop();
                bananas.RemoveAt(bananas.Count - 1);
            }
          
            if (nbSlaps >= 25)
            {
                controller.HasFailed = false;

                cakeShine.SetActive(true);
            }

            //plateRb.rotation -= tiltIncrement;
            //currentTilt += tiltIncrement*Time.deltaTime;

            if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
            {
                Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
                sound.GetComponent<AudioSource>().PlayOneShot(
                    sound.GetComponent<SoundSource>().Sounds[9]
                    );
            }
        }

        //plateRb.rotation += tiltForce*Time.deltaTime;

    }
}
