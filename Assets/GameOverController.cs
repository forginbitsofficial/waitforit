using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(WaitAndGoToNextMinigame());

        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
            sound.GetComponent<AudioSource>().PlayOneShot(
                sound.GetComponent<SoundSource>().Sounds[4]
                );
        }
    }

    //IEnumerator WaitAndGoToNextMinigame()
    //{
    //    yield return new WaitForSeconds(5f);

    //    GameData.LivesRemaining = GameData.StartLivesAmount;

    //    if (GameObject.FindGameObjectWithTag("LevelTransitioner") != null)
    //    {
    //        GameObject.FindGameObjectWithTag("LevelTransitioner").GetComponent<LevelTransitioner>().FadeIn("MainMenu");
    //    }
    //    else
    //    {
    //        SceneManager.LoadScene("MainMenu");
    //    }
    //}
}
