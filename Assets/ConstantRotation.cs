using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantRotation : MonoBehaviour
{
    public float rotSpeed;

    public float currentRot = 0f;

    // Update is called once per frame
    void Update()
    {
        currentRot += rotSpeed * Time.deltaTime;
        gameObject.transform.rotation = Quaternion.Euler(0, 0, currentRot);
    }
}
