using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomNudge : MonoBehaviour
{
    public float torqueAmp;
    public float translateAmp;

    // Start is called before the first frame update
    void Start()
    {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();

        rb.AddForce(UnityEngine.Random.Range(-translateAmp, translateAmp) * Vector2.right, ForceMode2D.Impulse);

        rb.AddTorque(UnityEngine.Random.Range(-torqueAmp, torqueAmp), ForceMode2D.Impulse);

        //rb.AddForce(translateAmp * Vector2.right, ForceMode2D.Impulse);

        //rb.AddTorque(torqueAmp, ForceMode2D.Impulse);
    }

}
