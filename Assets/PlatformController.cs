using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour
{
    public GameObject plate;

    public float moveSpeed = 1f;

    public Rigidbody2D plateRb;

    public float jumpForce;

    public bool canJump = true;

    public float jumpReloadTime = 2f;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.Q))
        {
            //plate.transform.position += moveSpeed * Time.deltaTime * Vector3.left;

            plateRb.position += moveSpeed * Time.deltaTime * Vector2.left;
            //currentTilt += tiltIncrement*Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            //plate.transform.position += moveSpeed * Time.deltaTime * Vector3.right;
            plateRb.position += moveSpeed * Time.deltaTime * Vector2.right;

            //currentTilt -= tiltIncrement * Time.deltaTime;

        }
        
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // JUMP

            if (canJump)
            {
                canJump = false; 
                plateRb.AddForce(jumpForce * Vector2.up, ForceMode2D.Impulse);

                StartCoroutine(WaitAndReloadJump());
            }
        }

        //plate.transform.rotation = Quaternion.Euler(0f, 0f, currentTilt);
    }

    IEnumerator WaitAndReloadJump()
    {

        yield return new WaitForSeconds(jumpReloadTime);

        canJump = true;

    }
}
