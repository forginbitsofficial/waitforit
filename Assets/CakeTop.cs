using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CakeTop : MonoBehaviour
{

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<Candle>() != null)
        {

            Debug.Log("GetCandle");
            collision.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            collision.gameObject.GetComponent<Candle>().FollowCake(transform);
        }
    }
}
