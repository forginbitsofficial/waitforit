using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandleController : MonoBehaviour
{

    public GameObject[] candles;

    public List<int> droppedCandles;

    public float waitTime;

    public int candlesDropped = 0;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LaunchCandle());
    }

    IEnumerator LaunchCandle()
    {
        if (droppedCandles.Count < 4)
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(1f, 2f));

            // DROP CANDLE

            int candleIndex;
            do
            {
                candleIndex = UnityEngine.Random.Range(0, candles.Length);

            } while (droppedCandles.Contains(candleIndex));

            droppedCandles.Add(candleIndex);

            candles[candleIndex].GetComponent<ConstantRotation>().enabled = false;
            candles[candleIndex].transform.rotation = Quaternion.identity;

            candles[candleIndex].GetComponent<Candle>().Drop();

            PlaySound();
           
            candlesDropped++;

            // RELAUNCH

            StartCoroutine(LaunchCandle());
        }
       

    }

    private void PlaySound()
    {

        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
            sound.GetComponent<AudioSource>().PlayOneShot(
                sound.GetComponent<SoundSource>().Sounds[14+candlesDropped]
                );
        }
    }
}
