using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public bool canJump = true;

    public float xApex;
    public float xRef;

    public float jumpTime = 0.5f;

    private void Start()
    {
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, xRef, gameObject.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {

        if (canJump)
        {


            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
                {
                    Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
                    sound.GetComponent<AudioSource>().PlayOneShot(
                        sound.GetComponent<SoundSource>().Sounds[6]
                        );
                }
                Debug.Log("Jump");

                canJump = false;

                LeanTween.moveY(gameObject, xApex, jumpTime).setEaseOutQuad().setLoopPingPong(1).setOnComplete(ReloadJump);
            }
        }
        
    }

    private void ReloadJump()
    {
        gameObject.transform.position = new Vector3(gameObject.transform.position.x,xRef, gameObject.transform.position.z);

        canJump = true;
    }
}
