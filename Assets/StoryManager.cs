using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StoryManager : MonoBehaviour
{

    public TextMeshProUGUI text;
    public TextMeshProUGUI text2;

    public GameObject img1;
    public GameObject img2;
    public GameObject img3;
    public GameObject img4;

    public UIController uicontroller;

    // Start is called before the first frame update
    void Start()
    {
        text.text = "Today was Mr Prizondoor's 50th birthday at the Wait For It Restaurant !";

        //LeanTween.moveLocalY(text.gameObject, -100f, 2f).setEaseOutQuad();

        //LeanTween.moveX(img1, 550f, 2f).setEaseOutQuad();

       

        StartCoroutine(Step1());

    }

    IEnumerator Step1()
    {
        yield return new WaitForSeconds(2f);

        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
            sound.GetComponent<AudioSource>().PlayOneShot(
                sound.GetComponent<SoundSource>().Sounds[19]
                );
        }

        yield return new WaitForSeconds(4f);

        text.text = "Chef Linguini had prepared a special cake for the grumpy man !";
        //LeanTween.moveX(img2, 550f, 2f).setEaseOutQuad();

       

        StartCoroutine(Step2());
    }

    IEnumerator Step2()
    {
        yield return new WaitForSeconds(2f);

        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
            sound.GetComponent<AudioSource>().PlayOneShot(
                sound.GetComponent<SoundSource>().Sounds[20]
                );
        }

        yield return new WaitForSeconds(4f);

        text.text = "As the waiter, all you had to do was serve the masterpiece...";
        //LeanTween.moveX(img3, 1370f, 2f).setEaseOutQuad();

       

        StartCoroutine(Step3());

    }

    IEnumerator Step3()
    {
        yield return new WaitForSeconds(2f);

        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
            sound.GetComponent<AudioSource>().PlayOneShot(
                sound.GetComponent<SoundSource>().Sounds[21]
                );
        }

        yield return new WaitForSeconds(4f);

        text.text = "But as luck would have it, something terrible happened...";
        //LeanTween.moveX(img4, 1370f, 2f).setEaseOutQuad();

       

        StartCoroutine(Step5());
    }


    IEnumerator Step5()
    {
        yield return new WaitForSeconds(2f);

        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
            sound.GetComponent<AudioSource>().PlayOneShot(
                sound.GetComponent<SoundSource>().Sounds[22]
                );
        }

        yield return new WaitForSeconds(4f);

        //LeanTween.moveY(text.gameObject, 1300f, 2f).setEaseOutQuad();

        //        LeanTween.moveY(text2.gameObject, 500f, 2f).setEaseOutQuad();

        text2.text = "And all you could do was delay the inevitable.";

      

        //LeanTween.moveX(img1, -500f, 2f).setEaseOutQuad();
        //LeanTween.moveX(img2, -500f, 2f).setEaseOutQuad();
        //LeanTween.moveX(img3, 2500f, 2f).setEaseOutQuad();
        //LeanTween.moveX(img4, 2500f, 2f).setEaseOutQuad();

        StartCoroutine(Step6());

    }

    IEnumerator Step6()
    {

        yield return new WaitForSeconds(2f);

        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
            sound.GetComponent<AudioSource>().PlayOneShot(
                sound.GetComponent<SoundSource>().Sounds[23]
                );
        }

        yield return new WaitForSeconds(4f);


        uicontroller.StartGame();

    }
}
