using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandlePosition : MonoBehaviour
{
    public float yMin;
    public float yMax;

    public GameObject target;
    public GameObject particles;

    // Start is called before the first frame update
    void Start()
    {
        particles.SetActive(false);

        transform.position = new Vector3(transform.position.x, UnityEngine.Random.Range(yMin, yMax));
    }

    public void Light()
    {
        if (target.activeInHierarchy)
        {
            if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
            {
                Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
                sound.GetComponent<AudioSource>().PlayOneShot(
                    sound.GetComponent<SoundSource>().Sounds[11]
                    );
            }
        }


        Debug.Log(
            "here");
        particles.SetActive(true);

        target.SetActive(false);
    }

}
