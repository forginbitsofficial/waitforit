using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public GameObject[] enemies;

    public float waitTime = 0.25f;

    private void Start()
    {
        foreach (GameObject gameObject in enemies)
        {
            gameObject.SetActive(false);
        }

        StartCoroutine(ShowEnemy());
    }

    IEnumerator ShowEnemy()
    {

        yield return new WaitForSeconds(waitTime);

        GameObject enemy = enemies[UnityEngine.Random.Range(0, enemies.Length)];

        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
            sound.GetComponent<AudioSource>().PlayOneShot(
                sound.GetComponent<SoundSource>().Sounds[10]
                );
        }

        enemy.SetActive(true);
        enemy.transform.localScale = new Vector3(enemy.transform.localScale.x, 0f);
        LeanTween.scaleY(enemy, 1f, 0.25f);

        yield return new WaitForSeconds(waitTime);

        enemy.GetComponent<Enemy>().Shoot();


        yield return new WaitForSeconds(waitTime);

        LeanTween.scaleY(enemy, 0f, waitTime);

        // RELAUNCH

        yield return new WaitForSeconds(waitTime);

        enemy.SetActive(false);

        StartCoroutine(ShowEnemy());



    }

}
