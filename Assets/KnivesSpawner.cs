using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnivesSpawner : MonoBehaviour
{
    public MinigameController mgcontroller;

    public GameObject bottlePrefab;

    public Vector3 force;
    public float torque;

    public float waitTime;

    public float yMin;
    public float yMax;

    public bool isFirstKnife = true;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ThrowBottle());
    }

    IEnumerator ThrowBottle()
    {
        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
            sound.GetComponent<AudioSource>().PlayOneShot(
                sound.GetComponent<SoundSource>().Sounds[13]
                );
        }

        if (isFirstKnife)
        {
            transform.position = new Vector3(transform.position.x, yMax, transform.position.z);

        }
        else
        {
            transform.position = new Vector3(transform.position.x, UnityEngine.Random.Range(0, 1f) < 0.5f ? yMin : yMax, transform.position.z);

        }

        isFirstKnife = false;

        GameObject bottle = Instantiate(bottlePrefab, transform.position, Quaternion.identity);

        bottle.GetComponent<Rigidbody2D>().AddForce(force, ForceMode2D.Impulse);
        bottle.GetComponent<Rigidbody2D>().AddTorque(torque, ForceMode2D.Impulse);
        bottle.GetComponent<DeathCollider>().controller = mgcontroller;

        yield return new WaitForSeconds(waitTime);

        StartCoroutine(ThrowBottle());

    }
}
