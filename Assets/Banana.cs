using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Banana : MonoBehaviour
{
    public float translateAmp;
    public float torqueAmp;

    public void Drop()
    {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();

        rb.constraints = RigidbodyConstraints2D.None;

        rb.AddForce(UnityEngine.Random.insideUnitCircle.normalized * translateAmp, ForceMode2D.Impulse);

        rb.AddTorque(UnityEngine.Random.Range(-torqueAmp, torqueAmp), ForceMode2D.Impulse);
    }
}
