using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public GameObject bulletPrefab;

    public float shootForce;


    public MinigameController mgController;

    public GameObject player;

    // Start is called before the first frame update
    public void Shoot()
    {
        Vector3 shootOrigin = transform.Find("Origin").position;

        Vector3 dir = Quaternion.Euler(0, 0, UnityEngine.Random.Range(-15f, 15f)) * (player.transform.position - shootOrigin).normalized;

        GameObject bullet = Instantiate(bulletPrefab, shootOrigin, Quaternion.identity);

        bullet.GetComponent<Rigidbody2D>().AddForce( dir * shootForce, ForceMode2D.Impulse);
        
        bullet.GetComponent<DeathCollider>().controller = mgController;

        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
            sound.GetComponent<AudioSource>().PlayOneShot(
                sound.GetComponent<SoundSource>().Sounds[5]
                );
        }
    }
}
