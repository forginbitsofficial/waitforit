using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitAndDestroy : MonoBehaviour
{
    public float waitTime;

    public GameObject animateObject;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Step1());

    }

    IEnumerator Step1()
    {
        yield return new WaitForSeconds(waitTime);

        LeanTween.moveY(animateObject, 1500f, 0.5f).setEaseInQuad().setDestroyOnComplete(true);

    }
}
