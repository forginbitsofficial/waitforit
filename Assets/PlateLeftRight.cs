using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateLeftRight : MonoBehaviour
{
    public GameObject plate;

    public float moveSpeed = 1f;

    public Rigidbody2D plateRb;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.Q))
        {
            //plate.transform.position += moveSpeed * Time.deltaTime * Vector3.left;

            plateRb.position += moveSpeed * Time.deltaTime * Vector2.left;
            //currentTilt += tiltIncrement*Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            //plate.transform.position += moveSpeed * Time.deltaTime * Vector3.right;
            plateRb.position += moveSpeed * Time.deltaTime * Vector2.right;

            //currentTilt -= tiltIncrement * Time.deltaTime;

        }

        //plate.transform.rotation = Quaternion.Euler(0f, 0f, currentTilt);
    }

   
}
