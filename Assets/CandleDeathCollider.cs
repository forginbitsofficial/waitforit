using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandleDeathCollider : MonoBehaviour
{
    public MinigameController controller;

    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Candle"))
        {
            Debug.Log("Death");
            controller.HasFailed = true;

            if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
            {
                Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
                sound.GetComponent<AudioSource>().PlayOneShot(
                    sound.GetComponent<SoundSource>().Sounds[2]
                    );
            }

            GameObject.FindGameObjectWithTag("MainUI").GetComponent<UIController>().ShowHit();

        }
    }
}
