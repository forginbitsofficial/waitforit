using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Candle : MonoBehaviour
{
    public Transform objectToFollow;

    public Vector3 offset;

   public void Drop()
    {
        GetComponent<Rigidbody2D>().gravityScale = 1f;
    }

    private void Update()
    {
        if (objectToFollow != null)
        {
            transform.position = objectToFollow.position - offset ;
        }
    }

    internal void FollowCake(Transform objtransform)
    {
        if (objectToFollow == null)
        {
            if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
            {
                Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
                sound.GetComponent<AudioSource>().PlayOneShot(
                    sound.GetComponent<SoundSource>().Sounds[11]
                    );
            }
        }

        objectToFollow = objtransform;

        offset = objectToFollow.position - transform.position;


    }
}
