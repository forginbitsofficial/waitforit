using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottleManager : MonoBehaviour
{
    public GameObject[] enemies;

    public float waitTime = 0.25f;

    private void Start()
    {
        foreach (GameObject gameObject in enemies)
        {
            gameObject.SetActive(false);
        }

        StartCoroutine(ShowEnemy());
    }

    IEnumerator ShowEnemy()
    {

        yield return new WaitForSeconds(waitTime);

        GameObject enemy = enemies[UnityEngine.Random.Range(0, enemies.Length)];

        enemy.SetActive(true);

        yield return new WaitForSeconds(waitTime);

        enemy.GetComponent<BottleSpawner>().Shoot();

        yield return new WaitForSeconds(waitTime);

        enemy.SetActive(false);

        // RELAUNCH

        yield return new WaitForSeconds(waitTime);

        StartCoroutine(ShowEnemy());



    }
}
