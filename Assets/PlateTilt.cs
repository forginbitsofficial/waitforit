using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateTilt : MonoBehaviour
{

    public GameObject plate;

    public float currentTilt = 0f;

    public float tiltIncrement = 1f;

    public Rigidbody2D plateRb;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.Q))
        {
            plateRb.rotation += tiltIncrement * Time.deltaTime;
            //currentTilt += tiltIncrement*Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            plateRb.rotation -= tiltIncrement * Time.deltaTime;

            //currentTilt -= tiltIncrement * Time.deltaTime;

        }

        //plate.transform.rotation = Quaternion.Euler(0f, 0f, currentTilt);
    }
}
