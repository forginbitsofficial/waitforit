using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSway : MonoBehaviour
{
    public Rigidbody2D rb;

    public Vector3 appliedForce;

    public float swayForce = 10f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        appliedForce = UnityEngine.Random.insideUnitCircle;

        StartCoroutine(ChangeSway());
    }

    IEnumerator ChangeSway()
    {

        yield return new WaitForSeconds(UnityEngine.Random.Range(1f, 5f));

        appliedForce = UnityEngine.Random.insideUnitCircle;
    }

    private void Update()
    {
        rb.AddForce(swayForce * appliedForce * Time.deltaTime);
    }
}
