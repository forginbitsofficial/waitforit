using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPositionChanger : MonoBehaviour
{

    public List<Vector3> positions;

    public GameObject[] pos;

    public int currentPositionIndex = 0;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.A))
        {
            if (currentPositionIndex == 0)
            {
            }
            else
            {
                currentPositionIndex--;
            }

            PlaySound();
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            if (currentPositionIndex == positions.Count - 1)
            {
            }
            else
            {
                currentPositionIndex++;
            }

            PlaySound();

        }

        for (int i = 0; i < pos.Length; i++)
        {
            if (i == currentPositionIndex)
            {
                pos[i].SetActive(true);
            }
            else
            {
                pos[i].SetActive(false);

            }
        }


        //gameObject.transform.position = positions[currentPositionIndex];
    }

    private void PlaySound()
    {
        switch (currentPositionIndex)
        {
            case 0:
                if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
                {
                    Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
                    sound.GetComponent<AudioSource>().PlayOneShot(
                        sound.GetComponent<SoundSource>().Sounds[6]
                        );
                }
                break;
            case 1:
                if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
                {
                    Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
                    sound.GetComponent<AudioSource>().PlayOneShot(
                        sound.GetComponent<SoundSource>().Sounds[7]
                        );
                }
                break;
            case 2:
                if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
                {
                    Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
                    sound.GetComponent<AudioSource>().PlayOneShot(
                        sound.GetComponent<SoundSource>().Sounds[8]
                        );
                }
                break;
            default:
                break;
        }
    }
}
