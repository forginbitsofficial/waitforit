using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChargeAndJump : MonoBehaviour
{
    public bool canJump = true;

    public float xApex;
    public float xRef;

    public float jumpTime = 0.5f;

    public float currentCharge = 0f;

    public float chargeMax = 10f;

    public float chargeIncrement = 100f;

    public Slider slider;

    private void Start()
    {
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, xRef, gameObject.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {

        if (canJump)
        {

            if (Input.GetKey(KeyCode.Space))
            {
                if (currentCharge + chargeIncrement * Time.deltaTime < chargeMax)
                {
                    currentCharge += chargeIncrement * Time.deltaTime;
                }
                else
                {
                    currentCharge = chargeMax;
                }
            }
            else if (Input.GetKeyUp(KeyCode.Space))
            {
                // JUMP

                if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
                {
                    Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
                    sound.GetComponent<AudioSource>().PlayOneShot(
                        sound.GetComponent<SoundSource>().Sounds[6]
                        );
                }

                Debug.Log("Jump");

                canJump = false;

                LeanTween.moveY(gameObject, xRef + (xApex - xRef) * (currentCharge / chargeMax), jumpTime * (0.5f + 0.5f*(currentCharge / chargeMax))).setEaseOutQuad().setLoopPingPong(1).setOnComplete(ReloadJump);

            }


        }

        slider.value = (currentCharge / chargeMax);
    }

    private void ReloadJump()
    {
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, xRef, gameObject.transform.position.z);

        canJump = true;

        currentCharge = 0f;

    }
}