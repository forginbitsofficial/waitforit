using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLighter : MonoBehaviour
{
    public MinigameController controller;

    public int nbCandlesLit = 0;

    public float verticalSpeed = 10f;

    public float horizontalSpeed = 10f;

    public float xMin;
    public float xMax;

    public float yMin;
    public float yMax;

    bool goesLeft = true;

    // Start is called before the first frame update
    void Start()
    {
        controller.HasFailed = true;

    }

    // Update is called once per frame
    void Update()
    {
        if (nbCandlesLit == 4)
        {
            controller.HasFailed = false;
        }

        if (transform.position.y < yMax)
        {
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.W))
            {

                transform.position += verticalSpeed * Time.deltaTime * Vector3.up;

            }
        }

        if (transform.position.y > yMin)
        {
            if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
            {
                transform.position += verticalSpeed * Time.deltaTime * Vector3.down;
            }
        }


        if (transform.position.x < xMin)
        {
            goesLeft = false;
        }

        if (transform.position.x > xMax)
        {
            goesLeft = true;
        }

        if (goesLeft)
        {
            transform.position += horizontalSpeed * Time.deltaTime * Vector3.left;

        }
        else
        {
            transform.position += horizontalSpeed * Time.deltaTime * Vector3.right;

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Cake"))
        {
            if (collision.transform.parent.GetComponent<CandlePosition>() != null)
            {
                collision.transform.parent.GetComponent<CandlePosition>().Light();
            }

            //collision.gameObject.SetActive(false);
            Debug.Log("Lighting");

            nbCandlesLit++;
        }
    }
}
