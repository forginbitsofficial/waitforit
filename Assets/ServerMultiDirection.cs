using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerMultiDirection : MonoBehaviour
{

    public float speed;

    public GameObject player;

    public float waitTimeMin;
    public float waitTimeMax;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody2D>().AddForce(speed * UnityEngine.Random.insideUnitCircle.normalized, ForceMode2D.Impulse);

        StartCoroutine(ChangeDirection());
    }

    IEnumerator ChangeDirection()
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(waitTimeMin, waitTimeMax));

        float rnd = UnityEngine.Random.Range(0, 1);

        if (rnd < 0.33f)
        {
            GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            GetComponent<Rigidbody2D>().AddForce(speed * (player.transform.position - transform.position).normalized, ForceMode2D.Impulse);
        }
        else if (rnd < 0.66f)
        {
            GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            GetComponent<Rigidbody2D>().AddForce(speed * (new Vector3(0, 0, 0) - transform.position).normalized, ForceMode2D.Impulse);
        }
        else
        {
            GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            GetComponent<Rigidbody2D>().AddForce(speed * UnityEngine.Random.insideUnitCircle.normalized, ForceMode2D.Impulse);

        }

        StartCoroutine(ChangeDirection());

    }

}
