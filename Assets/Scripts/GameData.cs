using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameData
{
    public static int NbDeaths = 0;

    public static bool IsMusicOn = true;

    public static bool IsSoundOn = true;

    public static bool GamePaused = false;

    public static bool IsSlowMotionAllowed = true;

    public static bool DisplayTaunt = false;

    public static bool IsNormalLeaderboardDisplayed = true;

    public static int CurrentLevel = 0;

    public static int LivesRemaining = 5;

    public static int StartLivesAmount = 5;

    public static bool HasAlreadySeenStory = false;

    public static int PreviousMinigameIndex = 0;

    public static List<int> PreviousMinigames = new List<int>();

    public static List<string> Taunts = new List<string>()
    {
        "Here is an <color=#FFFF00>easy</color> level to boost your <color=#FFFF00>confidence</color>",
        "This is the game that separates <color=#FFFF00>kids</color> from <color=#FFFF00>adults</color>",
         "Have you heard about the concept of <color=#FFFF00>difficulty curve</color> ? Let me show you",
        "There is no <color=#FFFF00>happy</color> ending",
         "You do realize this is a <color=#FFFF00>waste</color> of time ?",
        "This will put your <color=#FFFF00>patience</color> to the test",
         "This is the <color=#FFFF00>hardest</color> level",
        "That was a <color=#FFFF00>lie</color>. <color=#FFFF00>This</color> is the hardest level",
         "Tip : stop <color=#FFFF00>touching</color> the blue balls",
        "I enjoy making you <color=#FFFF00>suffer</color>",
         "That was more of a <color=#FFFF00>tutorial</color> than an actual level",
        "Turns out you're <color=#FFFF00>not</color> as good as you thought you were",
         "You <color=#FFFF00>hate</color> this game. Well it hates <color=#FFFF00>you</color> too",
        "Go back to playing <color=#FFFF00>marbles</color>",
         "About <color=#FFFF00>3.2 million</color> players have scored better than you at this point",
          "You are doing <color=#FFFF00>great</color> for someone who plays blindfolded",
        "Hold on. About <color=#FFFF00>5 hours</color> left",
         "<color=#FFFF00>Insanity</color> is doing the same thing over and over again and expecting different <color=#FFFF00>results</color>",
        "With you, I am running out of <color=#FFFF00>taunts</color>",
         "Why are you doing <color=#FFFF00>that</color> to yourself ?",
        "This is a game that you can play with <color=#FFFF00>all</color> your friends. A <color=#FFFF00>solo</color> game",
         "Please don't take your <color=#FFFF00>frustration</color> out on your keyboard",
        "Let's make things a bit more <color=#FFFF00>complicated</color>",
        "Don't you have something <color=#FFFF00>better</color> to do ?",
         "Would be so <color=#FFFF00>fun</color> if the game crashed now",
        "I guess it will take you longer to <color=#FFFF00>finish</color> the game than it took me to <color=#FFFF00>make</color> it",
         "It's okay to <color=#FFFF00>quit</color> now. <color=#FFFF00>Nobody</color> will miss you anyway",
         "What if there were <color=#FFFF00>more</color> than 30 levels ?",
        "Would be a <color=#FFFF00>shame</color> to be stuck at level 29",
         "The final level is <color=#FF0000>impossible</color>",
        "You should have picked the <color=#FFFF00>easy mode</color>"
    };

    public static void AddDeath()
    {
        NbDeaths++;
        Debug.Log("Player dead");
    }

    public static float TotalTime = 0f;

}
