using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicSource : MonoBehaviour
{
    private AudioSource masterAudioSource;
    private GameObject[] other;
    private bool NotFirst = false;

    private void Awake()
    {
        other = GameObject.FindGameObjectsWithTag("Music");

        foreach (GameObject oneOther in other)
        {
            if (oneOther.scene.buildIndex == -1)
            {
                NotFirst = true;
            }
        }

        if (NotFirst == true)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(transform.gameObject);
        masterAudioSource = GetComponents<AudioSource>()[0];
    }

    public void PlayMusic()
    {
        if (masterAudioSource.isPlaying) return;
        masterAudioSource.Play();
    }

    public void StopMusic()
    {
        masterAudioSource.Stop();
    }

}
