using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyOnLoad : MonoBehaviour
{
    public string tagNotToDestroy;

    private void Awake()
    {
        bool NotFirst = false;

        GameObject[] other = GameObject.FindGameObjectsWithTag(tagNotToDestroy);

        foreach (GameObject oneOther in other)
        {
            if (oneOther.scene.buildIndex == -1)
            {
                NotFirst = true;
            }
        }

        if (NotFirst == true)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(transform.gameObject);



    }
}
