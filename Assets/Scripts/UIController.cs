using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public GameObject PauseMenu;

    public GameObject SoundOffIcon;
    public GameObject SoundOnIcon;
    public GameObject MusicOffIcon;
    public GameObject MusicOnIcon;

    public string firstSceneName;

    public GameObject TauntPanel;

    public GameObject LevelSelectionPanel;
    public GameObject EasyModePanel;


    public GameObject Leaderboard;
    public GameObject HomePanel;

    public GameObject SendHighScoreButton;

    public TextMeshProUGUI ScoreText;
    public TextMeshProUGUI DeathsFinalScore;

    public TextMeshProUGUI PlayerName;


    public List<AudioClip> Sounds;

    public GameObject EasyTip;

    public GameObject NormalLeaderboard;
    public GameObject EasyLeaderboard;
    public GameObject NormalLeaderboardButton;
    public GameObject EasyLeaderboardButton;

    public GameObject menuPrompt;

    public TextMeshProUGUI nbLivesRemainingText;
    public TextMeshProUGUI currentLevelText;

    public float previousTimeScale;

    LevelTransitioner lt;

    public GameObject cross1;
    public GameObject cross2;
    public GameObject cross3;
    public GameObject cross4;
    public GameObject cross5;

    public GameObject hit;

    public TMP_InputField playerNameInput;

    internal void ShowHit()
    {
        if (hit != null)
        {
            Image img = hit.GetComponent<Image>();
            hit.SetActive(true);
            LeanTween.value(hit, 1f, 0f, 0.5f).setOnUpdate((float val) =>
             { img.color = new Color(1f, 1f, 1f, val); }).setOnComplete(HideHit);
        }

    }

    private void HideHit()
    {
        if (hit != null)
        {
            hit.SetActive(false);

        }
    }

    public void OpenStoryScene()
    {
        Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
        sound.GetComponent<AudioSource>().PlayOneShot(
            sound.GetComponent<SoundSource>().Sounds[0]
            );

        if (!GameData.HasAlreadySeenStory)
        {
            GameData.HasAlreadySeenStory = true;

            lt.FadeIn("StoryScene");
        }
        else
        {
            StartGame();
            //lt.FadeIn("InterScene");

        }

        //StartGame();

    }


    public void StartGame()
    {
        Debug.Log("Starting game");

        GameData.LivesRemaining = GameData.StartLivesAmount;
        GameData.CurrentLevel = 0;

        if (lt != null)
        {
            lt.FadeIn("InterScene");

        }
        else
        {
            SceneManager.LoadScene("InterScene");

        }
    }


    public void OpenMenuPrompt()
    {
        if (menuPrompt.activeInHierarchy == false)
        {
            PauseGame();

            //Time.timeScale = 0f;
            menuPrompt.SetActive(true);
        }
        else
        {
            UnpauseGame();

            menuPrompt.SetActive(false);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (playerNameInput != null)
            {
                if (PlayerName.text.Length > 0)
                {
                    SendHighScoreAndGoBackToMenu();
                }

            }
        }
    }

    public TextMeshProUGUI CakeSalvationScore;

    // Start is called before the first frame update
    void Start()
    {

        if (playerNameInput != null)
        {
            playerNameInput.Select();
            playerNameInput.ActivateInputField();
        }

        if (CakeSalvationScore != null)
        {
            CakeSalvationScore.text = "Cake Salvation Score : " + GameData.CurrentLevel;
        }


        if (GameObject.FindGameObjectWithTag("LevelTransitioner") != null)
        {
            lt = GameObject.FindGameObjectWithTag("LevelTransitioner").GetComponent<LevelTransitioner>();
        }

        // UPDATE NB LIVES
        if (nbLivesRemainingText != null)
        {
            nbLivesRemainingText.text = "Chef's hats left : " + GameData.LivesRemaining.ToString();
        }

        if (currentLevelText != null)
        {
            currentLevelText.text = "Cake Salvation Score : " + GameData.CurrentLevel.ToString();
        }

        if (cross1 != null)
        {
            if (GameData.LivesRemaining <= 0)
            {
                cross1.SetActive(true);

                cross1.transform.localScale = new Vector3(cross1.transform.localScale.x, 0f);
                LeanTween.scaleY(cross1, 1f, 0.3f);
            }
            else
            {
                cross1.SetActive(false);
            }
        }

        if (cross2 != null)
        {
            if (GameData.LivesRemaining <= 1)
            {
                cross2.SetActive(true);

                cross2.transform.localScale = new Vector3(cross2.transform.localScale.x, 0f);
                LeanTween.scaleY(cross2, 1f, 0.3f);
            }
            else
            {
                cross2.SetActive(false);
            }
        }

        if (cross3 != null)
        {
            if (GameData.LivesRemaining <= 2)
            {
                cross3.SetActive(true);

                cross3.transform.localScale = new Vector3(cross3.transform.localScale.x, 0f);
                LeanTween.scaleY(cross3, 1f, 0.3f);
            }
            else
            {
                cross3.SetActive(false);
            }
        }


        if (cross4 != null)
        {
            if (GameData.LivesRemaining <= 3)
            {
                cross4.SetActive(true);

                cross4.transform.localScale = new Vector3(cross4.transform.localScale.x, 0f);
                LeanTween.scaleY(cross4, 1f, 0.3f);
            }
            else
            {
                cross4.SetActive(false);
            }
        }

        if (cross5 != null)
        {
            if (GameData.LivesRemaining <= 4)
            {
                cross5.SetActive(true);

                cross5.transform.localScale = new Vector3(cross5.transform.localScale.x, 0f);
                LeanTween.scaleY(cross5, 1f, 0.3f);
            }
            else
            {
                cross5.SetActive(false);
            }
        }





        if (GameData.DisplayTaunt)
        {
            StartCoroutine(DisplayTaunt());
            //DisplayTaunt();
        }


        if (ScoreText != null)
        {
            ScoreText.text = FormatTime(GameData.TotalTime);
        }

        if (DeathsFinalScore != null)
        {
            DeathsFinalScore.text = GameData.NbDeaths.ToString();
        }


        if (GameData.IsSoundOn)
        {
            if (SoundOnIcon != null && SoundOffIcon != null)
            {
                SoundOnIcon.SetActive(true);

                SoundOffIcon.SetActive(false);
            }

        }
        else
        {
            if (SoundOnIcon != null && SoundOffIcon != null)
            {
                SoundOnIcon.SetActive(false);

                SoundOffIcon.SetActive(true);
            }
        }

        if (GameData.IsMusicOn)
        {
            if (MusicOnIcon != null && MusicOffIcon != null)
            {
                MusicOnIcon.SetActive(true);
                MusicOffIcon.SetActive(false);
            }
        }
        else
        {
            if (MusicOnIcon != null && MusicOffIcon != null)
            {
                MusicOnIcon.SetActive(false);
                MusicOffIcon.SetActive(true);
            }
        }


        if (EasyTip != null)
        {
            if (GameData.IsSlowMotionAllowed)
            {
                EasyTip.SetActive(true);
            }
            else
            {
                EasyTip.SetActive(false);

            }
        }
    }


    public string FormatTime(float totalTimeInSeconds)
    {
        int hours = (int)totalTimeInSeconds / 3600;
        int minutes = (int)((int)totalTimeInSeconds - 3600 * hours) / 60;
        int seconds = (int)((int)totalTimeInSeconds - 3600 * hours - 60 * minutes);
        int milliseconds = (int)((totalTimeInSeconds - Mathf.Floor(totalTimeInSeconds)) * 1000f);
        return string.Format("{0:00}:{1:00}:{2:00}:{3:000}", hours, minutes, seconds, milliseconds);
    }

    IEnumerator DisplayTaunt()
    {
        if (TauntPanel != null)
        {
            TauntPanel.SetActive(true);

            yield return new WaitForSecondsRealtime(3f);

            TauntPanel.SetActive(false);

            GameData.DisplayTaunt = false;
        }
    }

    //public void RestartLevel()
    //{
    //    Scene scene = SceneManager.GetActiveScene();
    //    SceneManager.LoadScene(scene.name);
    //}

    public void PauseGame()
    {
        Debug.Log("Pause game");

        GameData.GamePaused = true;


        previousTimeScale = Time.timeScale;

        Debug.Log("previousTimeScale : " + previousTimeScale);


        Time.timeScale = 0f;

        if (PauseMenu != null)
        {
            PauseMenu.SetActive(true);

        }

        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
            sound.GetComponent<AudioSource>().PlayOneShot(
                sound.GetComponent<SoundSource>().Sounds[0]
                );
        }
    }

    public void UnpauseGame()
    {
        GameData.GamePaused = false;

        Time.timeScale = previousTimeScale;

        Debug.Log("timescale : " + Time.timeScale);

        if (PauseMenu != null)
        {
            PauseMenu.SetActive(false);
        }

        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
            sound.GetComponent<AudioSource>().PlayOneShot(
                sound.GetComponent<SoundSource>().Sounds[0]
                );
        }
    }

    public void ToggleSound()
    {

        if (GameData.IsSoundOn)
        {
            GameData.IsSoundOn = false;

            SoundOnIcon.SetActive(false);
            SoundOffIcon.SetActive(true);
            if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
            {
                GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer").GetComponent<AudioSource>().mute = true;
            }
        }
        else
        {
            GameData.IsSoundOn = true;
            SoundOnIcon.SetActive(true);
            SoundOffIcon.SetActive(false);
            if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
            {
                GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer").GetComponent<AudioSource>().mute = false;
            }
        }
        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer").GetComponent<AudioSource>().PlayOneShot(Sounds[0]);
        }
    }

    public void ToggleMusic()
    {
        if (GameData.IsMusicOn)
        {
            GameData.IsMusicOn = false;

            MusicOnIcon.SetActive(false);
            MusicOffIcon.SetActive(true);
            if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
            {
                GameObject.FindGameObjectsWithTag("Music")[0].GetComponents<AudioSource>()[0].mute = true;
                //GameObject.FindGameObjectsWithTag("Music")[0].GetComponents<AudioSource>()[1].mute = true;
            }
        }
        else
        {
            GameData.IsMusicOn = true;
            MusicOnIcon.SetActive(true);
            MusicOffIcon.SetActive(false);
            if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
            {
                GameObject.FindGameObjectsWithTag("Music")[0].GetComponents<AudioSource>()[0].mute = false;
                // GameObject.FindGameObjectsWithTag("Music")[0].GetComponents<AudioSource>()[1].mute = false;
            }
        }

        GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer").GetComponent<AudioSource>().PlayOneShot(Sounds[0]);

    }

    public void OpenWebsite()
    {
        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
            sound.GetComponent<AudioSource>().PlayOneShot(
                sound.GetComponent<SoundSource>().Sounds[0]
                );
        }

        Application.OpenURL("http://www.forginbits.com/");


    }

    public void OpenMenu()
    {
        Debug.Log("Open menu");


        PauseMenu.SetActive(true);
        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer").GetComponent<AudioSource>().PlayOneShot(Sounds[0]);
        }
    }

    public void PlayMoreGames()
    {
        Debug.Log("Play more games");

        OpenWebsite();
        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer").GetComponent<AudioSource>().PlayOneShot(Sounds[0]);
        }
    }


    //public void OpenLevelsPanel()
    //{
    //    HomePanel.SetActive(false);
    //    LevelSelectionPanel.SetActive(true);

    //    if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
    //    {
    //        GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer").GetComponent<AudioSource>().PlayOneShot(Sounds[0]);
    //    }

    //}

    //public void StartLevel(int levelIndex)
    //{
    //    if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
    //    {
    //        GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer").GetComponent<AudioSource>().PlayOneShot(Sounds[0]);
    //    }

    //    GameData.IsSlowMotionAllowed = false;

    //    GameData.DisplayTaunt = true;
    //    SceneManager.LoadScene("Level " + levelIndex);


    //}

    //public void StartNormalMode()
    //{
    //    if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
    //    {
    //        GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer").GetComponent<AudioSource>().PlayOneShot(Sounds[0]);
    //    }
    //    GameData.IsSlowMotionAllowed = false;

    //    GameData.DisplayTaunt = true;
    //    SceneManager.LoadScene(firstSceneName);


    //}

    //public void OpenEasyModePanel()
    //{
    //    HomePanel.SetActive(false);

    //    EasyModePanel.SetActive(true);
    //    if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
    //    {
    //        GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer").GetComponent<AudioSource>().PlayOneShot(Sounds[0]);
    //    }

    //}

    //public void StartEasyMode()
    //{
    //    if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
    //    {
    //        GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer").GetComponent<AudioSource>().PlayOneShot(Sounds[0]);
    //    }
    //    GameData.IsSlowMotionAllowed = true;

    //    GameData.DisplayTaunt = true;
    //    SceneManager.LoadScene(firstSceneName);

    //}

    public void BackToStart()
    {
        LevelSelectionPanel.SetActive(false);
        HomePanel.SetActive(true);

        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer").GetComponent<AudioSource>().PlayOneShot(Sounds[0]);
        }

    }

    public void SendHighScoreAndGoBackToMenu()
    {
        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
            sound.GetComponent<AudioSource>().PlayOneShot(
                sound.GetComponent<SoundSource>().Sounds[0]
                );
        }

        HighScores.UploadScore(PlayerName.text, GameData.CurrentLevel);

        // CONVERT HIGH SCORE TO INT

        //float time = GameData.TotalTime;

        //float nbMilliseconds = GameData.TotalTime * 1000f;
        //int bigInt = (int)nbMilliseconds;

        //int convertedTime = 1000000000 - bigInt;

        //HighScores.UploadScore(PlayerName.text, convertedTime);

        //GameData.NbDeaths = 0;
        //GameData.TotalTime = 0;


        if (lt != null)
        {
            lt.FadeIn("MainMenu");
        }
        else
        {
            SceneManager.LoadScene("MainMenu");
        }
    }

    [ContextMenu("TestScore")]
    public void TestScore()
    {
        //HighScores.UploadScore("Test" + UnityEngine.Random.Range(3000, 4000).ToString(), UnityEngine.Random.Range(3000, 4000));

    }

    public void SetName(string name)
    {
        if (name.Length > 0)
        {
            SendHighScoreButton.GetComponent<Button>().interactable = true;
            SendHighScoreButton.SetActive(true);
        }
        else
        {
            SendHighScoreButton.GetComponent<Button>().interactable = false;
            SendHighScoreButton.SetActive(false);

        }


    }

    public void OpenLeaderboards()
    {
        HomePanel.SetActive(false);

        Leaderboard.SetActive(true);
        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer").GetComponent<AudioSource>().PlayOneShot(Sounds[0]);
        }
    }

    public void ExitToMenu()
    {
        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer").GetComponent<AudioSource>().PlayOneShot(Sounds[0]);
        }
        LeanTween.cancelAll();

        UnpauseGame();

        GameData.NbDeaths = 0;
        GameData.TotalTime = 0;

        if (lt != null)
        {
            lt.FadeIn("MainMenu");
        }
        else
        {
            SceneManager.LoadScene("MainMenu");
        }

    }

    public void OpenEasyLeaderboard()
    {
        GameData.IsNormalLeaderboardDisplayed = false;


        NormalLeaderboard.SetActive(false);
        EasyLeaderboard.SetActive(true);

        NormalLeaderboardButton.transform.Find("MenuLabel").GetComponent<TextMeshProUGUI>().color = Color.white;
        EasyLeaderboardButton.transform.Find("MenuLabel").GetComponent<TextMeshProUGUI>().color = Color.yellow;
    }

    public void OpenNormalLeaderboard()
    {
        GameData.IsNormalLeaderboardDisplayed = true;

        NormalLeaderboard.SetActive(true);
        EasyLeaderboard.SetActive(false);

        NormalLeaderboardButton.transform.Find("MenuLabel").GetComponent<TextMeshProUGUI>().color = Color.yellow;
        EasyLeaderboardButton.transform.Find("MenuLabel").GetComponent<TextMeshProUGUI>().color = Color.white;
    }

}
