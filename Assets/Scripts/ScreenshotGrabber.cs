﻿#if UNITY_EDITOR
using System.IO;
using UnityEditor;
using UnityEngine;

public class ScreenshotGrabber : MonoBehaviour
{

    private static int index;


    [MenuItem("Screenshot/Grab")]
    public static void Grab()
    {
        ScreenCapture.CaptureScreenshot(@"Screenshots\Screenshot" + index.ToString() + ".png", 1);

        index++;
    }
}

#endif
