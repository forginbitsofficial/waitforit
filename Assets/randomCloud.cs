using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class randomCloud : MonoBehaviour
{
    public float moveSpeed;

    public Vector3 dir;

    public float xMin;
    public float xMax;

    public bool orientation;

    // Start is called before the first frame update
    void Start()
    {
        //bool orientation = UnityEngine.Random.Range(0f,1f) < 0.5f;

        if (orientation)
        {
            dir = new Vector2(moveSpeed, 0f);
        } else
        {
            dir = new Vector2(-moveSpeed, 0f);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x > xMax)
        {
            transform.position = new Vector3(xMin, transform.position.y, transform.position.z);
        }
        else if (transform.position.x < xMin)
        {
            transform.position = new Vector3(xMax, transform.position.y, transform.position.z);
        }

        transform.position += Time.deltaTime * dir;
    }
}
