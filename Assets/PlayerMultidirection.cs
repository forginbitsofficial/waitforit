using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMultidirection : MonoBehaviour
{
    public GameObject plate;

    public float moveSpeed = 1f;

    public Rigidbody2D plateRb;

    private void Start()
    {
        if (GameObject.FindGameObjectsWithTag("Music").Length != 0)
        {
            Transform sound = GameObject.FindGameObjectsWithTag("Music")[0].transform.Find("SoundPlayer");
            sound.GetComponent<AudioSource>().PlayOneShot(
                sound.GetComponent<SoundSource>().Sounds[18]
                );
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.Q))
        {
            //plate.transform.position += moveSpeed * Time.deltaTime * Vector3.left;

            plateRb.position += moveSpeed * Time.deltaTime * Vector2.left;
            //currentTilt += tiltIncrement*Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            //plate.transform.position += moveSpeed * Time.deltaTime * Vector3.right;
            plateRb.position += moveSpeed * Time.deltaTime * Vector2.right;

            //currentTilt -= tiltIncrement * Time.deltaTime;

        }
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.W))
        {
            plateRb.position += moveSpeed * Time.deltaTime * Vector2.up;

        }
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.S))
        {
            plateRb.position += moveSpeed * Time.deltaTime * Vector2.down;

        }

        //plate.transform.rotation = Quaternion.Euler(0f, 0f, currentTilt);
    }
}
